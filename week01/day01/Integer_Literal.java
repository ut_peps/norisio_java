public class Integer_Literal{
	public static void main(String[] args){
		System.out.println("数値リテラルと実際の値");
		System.out.println("10進数で10 = " + 10);
		System.out.println("8進数で012 = "+ 012);
		System.out.println("16進数で0xA=" + 0xA);
		System.out.println("2進数で0b1010=" + 0b1010);
		System.out.println("浮動小数点リテラルと実際の値");
		System.out.println("floatで10.0F="+10.0F);
		System.out.println("doubleで10.0 = "+10.0);
	}
}

