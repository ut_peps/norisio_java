public class Character_Literal{
	public static void main(String[] args){
		System.out.println("'A' : " + 'A');   //A
		System.out.println("'A'+'B'+'C'"  +  'A'+'B'+'C'); //ABC
		System.out.println("文字列リテラル " + "ABC");  //ABC
		System.out.println("boolean型リテラル" + true); //true
	}
}
