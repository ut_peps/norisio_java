public class Swap {
  static public void main(String args[]) {
    int x = 10;
    int y = 20;
    System.out.println("x : " + x);
    System.out.println("y : " + y);
      int temp = x;
    x = y;
    y = temp;
    System.out.println("after swapped-----");
    System.out.println("x : " + x);
    System.out.println("y : " + y);

  }
}
