public class ReviewTest {
  static public void main(String args[]) {
    //byte x_Byte=256;  //byteは-128〜127
    byte x_Byte=126;
    int x_Int=150;
    int i;        
    long x_Long=15700000000l;   //Long数値リテラルはLをつける
    float y;      //宣言するだけのとき = はいらない
    //x_Int;      
    i = 200;
    System.out.println("i = "+i);
    System.out.println("x_Int = "+x_Int);
    System.out.println("x_Byte = "+x_Byte);
    System.out.println("x_Long = "+x_Long);
  }
}
