public class Q1 {
  static public void main(String args[]) {
    int rest = 10000 - 468;
    int[] unit = new int[]{5000, 1000, 500, 100, 50, 10, 5, 1};

    int i = 0;
    while(rest > 0){
      int result = 0;
      while(rest >= unit[i]){
        rest -= unit[i];
        result++;
      }
      System.out.println(unit[i] + "円" + (i<=1?"札":"玉") + "の枚数\t= " + result);
      i++;
    }
  }
}

