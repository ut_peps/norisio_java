public class Q1 {
  static public void main(String args[]) {
    int input = Integer.parseInt(args[0]);
    System.out.println((input >= 0 ? "正":"負") +"の" +(input % 2 == 0? "偶数":"奇数"));
  }
}
