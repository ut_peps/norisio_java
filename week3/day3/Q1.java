public class Q1 {
  static public void main(String args[]) {
    int x = Integer.parseInt(args[0]);
    switch (x) {
      case 0: 
        System.out.println("0が入力されました");
        break;
      case 1: 
        System.out.println("1が入力されました");
        break;
      case 2: 
        System.out.println("2が入力されました");
        break;
      default: 
        System.out.println("0から2の対応した数値を入力してください");
        break;
    }
  }
}
