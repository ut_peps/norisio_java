public class Q1 {
  static public void main(String args[]) {
    int month = Integer.parseInt(args[0]);
    switch (month) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        System.out.println("31日");
        break;
      case 4:
      case 6:
      case 9:
      case 11:
        System.out.println("30日");
        break;
      case 2:
        System.out.println("28日");
        break;
      default:
        System.out.println("0日");
    }
    //問題文の意味がよくわかりません…
  }
}
