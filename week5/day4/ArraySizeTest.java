public class ArraySizeTest {
  static public void main(String args[]) {
    int size = Integer.parseInt(args[0]);
    int[] array = new int[size];

    array[size-1] = 1;

    for(int i=0; i<size; i++){
      System.out.println("array["+i+"] = "+array[i]);
    }
  }
}
