public class q {
  static public void main(String args[]) {
    method1();
    method2();
    method3();
  }
  static void method1() {
    System.out.println("1");
  }
  static void method2() {
    System.out.println("2");
    method1();
  }
  static void method3() {
    System.out.println("3");
    method2();
  }
}
