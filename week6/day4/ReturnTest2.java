public class ReturnTest2 {
  static public void main(String args[]) {
    int num=Integer.parseInt(args[0]);
    System.out.println("入力された数字は"+checkOddEven(num)+"です");
  }
  static String checkOddEven(int num)  {
    return num%2 == 0 ? "偶数": "奇数";
  }
}
